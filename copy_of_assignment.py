# -*- coding: utf-8 -*-
"""Copy of Assignment.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1rxbrB_QSX1wQUVGIlicvUP1FX78kubRF
"""

#1 to find length of a longest string

def longest_word(string):

    words = string.split()

    longest = ''
    for word in words:
        if len(word) > len(longest):
            longest = word
    return longest + ' ' + str(len(longest))

string= input("enter a string")
longest = longest_word(string)
print(longest)

#2



def is_symmetrical(string):

    return string == string[::-1]


def is_palindrome(string):
    n = len(string)
    if n % 2 == 0:
        first_half = string[:n//2]
        second_half = string[n//2:]
    else:
        first_half = string[:n//2]
        second_half = string[n//2+1:]
    return first_half == second_half[::-1]

string = input("enter a string")
if is_symmetrical(string):
    print("The input string is symmetrical")
else:
    print("The input string is not symmetrical")
if is_palindrome(string):
 print("The input string is a palindrome")

else:

    print("The input string is not a palindrome")

#3

def is_pangram(input_str, alphabet):
    flag = True
    for char in alphabet:
        if char not in input_str.lower():
            flag = False
            break
    if flag:
        print("Yes")
    else:
        print("No")

input_str = "The quick brown fox jumps over the lazy dog"
alphabet = "abcdefghijklmnopqrstuvwxyz"
is_pangram(input_str, alphabet)

#4

input_str = input("enter a string")

words = input_str.split('-')

words.sort()
output_str = '-'.join(words)
print(output_str)

#5

def is_prime(n):
    if n <= 1:
        return False
    elif n <= 3:
        return True
    elif n % 2 == 0 or n % 3 == 0:
        return False
    i = 5
    while i * i <= n:
        if n % i == 0 or n % (i + 2) == 0:
            return False
        i += 6
    return True
def print_twin_primes(n):
    for i in range(3, n+1, 2):
        if is_prime(i) and is_prime(i+2):
            print(str(i) + ' and ' + str(i+2))
limit = int(input("Enter the limit: "))
print_twin_primes(limit)

#6
input_str = "now"
doubled_str = double_characters(input_str)
print(doubled_str) # Output: nnooww

input_str = "123a!"
doubled_str = double_characters(input_str)
print(doubled_str) # Output: 112233aa!!

#7

def fibonacci(n):
    fib_list = [0, 1]
    for i in range(2, n):
        next_fib = fib_list[-1] + fib_list[-2]
        fib_list.append(next_fib)
    return fib_list[:n]
n = int(input("Enter the number of terms: "))
print(fibonacci(n))

#8

num = int(input("Enter a number: "))
even_count = 0
odd_count = 0
while num != 0:
    rem = num % 10
    if rem % 2 == 0:
        even_count += 1
    else:
        odd_count += 1
    num = num // 10
print("even:", even_count, "odd:", odd_count)

#9

num = int(input("Enter a number: "))
sum = 0
for i in range(1, num):
    if num % i == 0:
        sum += i
if sum == num:
    print("True")
else:
    print("False")

#10
string = input("Enter a string: ")

words = string.split()

for word in words:
    if len(word) % 2 == 0:
        print(word)

#1